@echo off & SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
echo                                      ---WARNING---
echo.
echo                                   Moodle DEPLOY PROJECT!
set /p URL=Enter repository url:

if [%URL%] EQU [] goto error
:error
ECHO Repository url required !
exit

echo %URL%

set /p PROJECT_NAME=Enter project name (DON'T USE SYMBOL: '-_"\):

if [%PROJECT_NAME%] EQU [] goto error
:error
ECHO Project name required !
exit

echo %PROJECT_NAME%
set PROJECT_NAME=%PROJECT_NAME: =_%

git init
git remote add origin https://github.com/moodle/moodle.git
git pull origin MOODLE_311_STABLE
git remote add origin-new "%URL%"
git branch -M master
git push -uf origin-new master
git remote remove origin
git remote rename origin-new origin

cd local

call :tolower PROJECT_NAME
SET PROJECT_NAME
goto :EOF

:tolower
for %%L IN (a b c d e f g h i j k l m n o p q r s t u v w x y z) DO SET %1=!%1:%%L=%%L!

echo %PROJECT_NAME%

rem start change local plugin

move local_project_name.zip local\

cd local

"C:\Program Files\WinRAR\winrar.exe" x -O+ -IBCK local_project_name.zip \

rename "project_name" %PROJECT_NAME%

REM change version.php

Set infile=version.php

Set find=local_project_name

Set replace=local_%PROJECT_NAME%

echo %replace%

set COUNT=0
for /F "tokens=* delims=," %%n in (!infile!) do (
    set LINE=%%n
    set TMPR=!LINE:%find%=%replace%!
    Echo !TMPR!>>TMP.TXT
)

move TMP.TXT %infile%

REM change file in lang\en\

cd %PROJECT_NAME%\lang\en

rename local_project_name.php local_%PROJECT_NAME%.php

Set infile=local_%PROJECT_NAME%.php

Set find=Local project_name

Set replace=%PROJECT_NAME%

echo %replace%

set COUNT=0
for /F "tokens=* delims=," %%n in (!infile!) do (
    set LINE=%%n
    set TMPR=!LINE:%find%=%replace%!
    Echo !TMPR!>>TMP.TXT
)

move TMP.TXT %infile%

REM end change local plugin

cd ..\..\..\

REM Delete archive
DEL local_project_name.zip

cd ..

REM start change theme plugin

move theme_project_name.zip theme\

cd theme

"C:\Program Files\WinRAR\winrar.exe" x -O+ -IBCK theme_project_name.zip \

rename "project_name" %PROJECT_NAME%

REM change version.php

Set infile=version.php

Set find=theme_project_name

Set replace=theme_%PROJECT_NAME%

echo %replace%

set COUNT=0
for /F "tokens=* delims=," %%n in (!infile!) do (
    set LINE=%%n
    set TMPR=!LINE:%find%=%replace%!
    Echo !TMPR!>>TMP.TXT
)

move TMP.TXT %infile%

REM change file in lang\en\

cd %PROJECT_NAME%\lang\en

rename theme_project_name.php theme_%PROJECT_NAME%.php

Set infile=theme_%PROJECT_NAME%.php

Set find=Theme project_name

Set replace=%PROJECT_NAME%

echo %replace%

set COUNT=0
for /F "tokens=* delims=," %%n in (!infile!) do (
    set LINE=%%n
    set TMPR=!LINE:%find%=%replace%!
    Echo !TMPR!>>TMP.TXT
)

move TMP.TXT %infile%

REM end change theme plugin

cd ..\..\..\

REM Delete archive

DEL theme_project_name.zip
